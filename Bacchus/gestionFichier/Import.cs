﻿using System;
using System.Data.SQLite;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Bacchus.gestionFichier
{
    class Import
    {
        protected static string DbConnection = "Data Source=Bacchus.SQLite;Version=3;New=True;Compress=True;";

        public static int ImportCSV(String CSVFile, Boolean ModeEcrase, ProgressBar ProgBar)
        {
            int NumLines = 0;
            using (SQLiteConnection Connection = new SQLiteConnection(DbConnection))
            {
                Connection.Open();
                string[] Lines = File.ReadAllLines(CSVFile, Encoding.GetEncoding("latin1"));
                NumLines = Lines.GetLength(0);
                string[] Fields;
                Fields = Lines[0].Split(new char[] { ';' });
                int Cols = Fields.GetLength(0);

                SQLiteCommand Command = new SQLiteCommand(Connection);

                if (ModeEcrase == true)
                {
                    //En mode écrasement vider la BDD
                    dao.ArticleDao.DeleteArticles();
                    dao.MarqueDao.DeleteMarques();
                    dao.FamilleDao.DeleteFamilles();
                    dao.SousFamilleDao.DeleteSousFamille();
                }

                ProgBar.Maximum = NumLines;
                ProgBar.Step = 1;
                ProgBar.PerformStep();

                for (int i = 1; i < NumLines; i++)
                {
                    Fields = Lines[i].Split(new char[] { ';' });
                    String Description = Fields[0];
                    String RefArticle = Fields[1];
                    String MarqueNom = Fields[2];
                    String FamilleNom = Fields[3];
                    String SousFamilleNom = Fields[4];
                    float PrixHT = float.Parse(Fields[5], System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR"));

                    Famille Fam = new Famille(FamilleNom);
                    long RefFam = dao.FamilleDao.Persist(Fam);
                    Fam.RefFamille = RefFam;

                    Marque Mar = new Marque(MarqueNom);
                    long RefMarque = dao.MarqueDao.Persist(Mar);
                    Mar.RefMarque = RefMarque;

                    SousFamille SousFamille = new SousFamille(Fam, SousFamilleNom);
                    long RefSousFamille = dao.SousFamilleDao.Persist(SousFamille);
                    SousFamille.Reference = RefSousFamille;

                    Article article = new Article(RefArticle, Description, Mar, SousFamille, PrixHT, 0);
                    dao.ArticleDao.Persist(article);

                    ProgBar.PerformStep();

                }
            }
            return NumLines;
        }
    }
}
