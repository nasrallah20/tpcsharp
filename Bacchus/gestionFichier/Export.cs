﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bacchus.gestionFichier
{
    class Export
    {
        protected static string DbConnection = "Data Source=Bacchus.SQLite;Version=3;New=True;Compress=True;";
        public static void ExportBddEnCSV(string FileName, ProgressBar ProgBar)
        {

            FileStream Fs = File.OpenWrite(FileName);
            using (StreamWriter Sw = new StreamWriter(Fs, Encoding.GetEncoding("iso-8859-1")))
            {
                String[] Header = { "Description", "Ref", "Marque", "Famille", "Sous-Famille", "Prix H.T." };
                Sw.Write(String.Join(";", Header));

                var NumLines = dao.ArticleDao.GetAllArticles().Count();
                ProgBar.Maximum = NumLines;
                ProgBar.Step = 1;
                ProgBar.PerformStep();

                foreach (Article Article in dao.ArticleDao.GetAllArticles())
                {
                    String[] fields = { Article.Description, Article.RefArticle, Article.Marque.Nom, Article.SousFamille.Famille.Nom, Article.SousFamille.Nom, Article.PrixHT.ToString(), "\n" };
                    Sw.Write(String.Join(";", fields));
                    ProgBar.PerformStep();
                }

            }

            
        }
    }
}
