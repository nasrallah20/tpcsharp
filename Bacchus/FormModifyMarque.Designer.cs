﻿namespace Bacchus
{
    partial class FormModifyMarque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Confirmer = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.NomTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ReferenceTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Confirmer
            // 
            this.Confirmer.Location = new System.Drawing.Point(376, 213);
            this.Confirmer.Name = "Confirmer";
            this.Confirmer.Size = new System.Drawing.Size(104, 40);
            this.Confirmer.TabIndex = 15;
            this.Confirmer.Text = "Confirmer";
            this.Confirmer.UseVisualStyleBackColor = true;
            this.Confirmer.Click += new System.EventHandler(this.Confirmer_Click);
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(251, 213);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(104, 40);
            this.Cancel.TabIndex = 14;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // NomTextBox
            // 
            this.NomTextBox.Location = new System.Drawing.Point(190, 137);
            this.NomTextBox.Name = "NomTextBox";
            this.NomTextBox.Size = new System.Drawing.Size(290, 31);
            this.NomTextBox.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 25);
            this.label2.TabIndex = 12;
            this.label2.Text = "Nom";
            // 
            // ReferenceTextBox
            // 
            this.ReferenceTextBox.Location = new System.Drawing.Point(190, 74);
            this.ReferenceTextBox.Name = "ReferenceTextBox";
            this.ReferenceTextBox.ReadOnly = true;
            this.ReferenceTextBox.Size = new System.Drawing.Size(290, 31);
            this.ReferenceTextBox.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 25);
            this.label1.TabIndex = 17;
            this.label1.Text = "Référence";
            // 
            // FormModifyMarque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 300);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ReferenceTextBox);
            this.Controls.Add(this.Confirmer);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.NomTextBox);
            this.Controls.Add(this.label2);
            this.Name = "FormModifyMarque";
            this.Text = "Modifier une marque";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Confirmer;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.TextBox NomTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ReferenceTextBox;
        private System.Windows.Forms.Label label1;
    }
}