﻿namespace Bacchus
{
    partial class FormModifySousFam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.Confirmer = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.NomTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.FamilleTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ReferenceTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(72, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 25);
            this.label3.TabIndex = 19;
            this.label3.Text = "Famille";
            // 
            // Confirmer
            // 
            this.Confirmer.Location = new System.Drawing.Point(403, 290);
            this.Confirmer.Name = "Confirmer";
            this.Confirmer.Size = new System.Drawing.Size(132, 40);
            this.Confirmer.TabIndex = 17;
            this.Confirmer.Text = "Confirmer";
            this.Confirmer.UseVisualStyleBackColor = true;
            this.Confirmer.Click += new System.EventHandler(this.Confirmer_Click);
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(224, 290);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(140, 40);
            this.Cancel.TabIndex = 16;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // NomTextBox
            // 
            this.NomTextBox.Location = new System.Drawing.Point(209, 204);
            this.NomTextBox.Name = "NomTextBox";
            this.NomTextBox.Size = new System.Drawing.Size(326, 31);
            this.NomTextBox.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 210);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 25);
            this.label2.TabIndex = 14;
            this.label2.Text = "Nom";
            // 
            // FamilleTextBox
            // 
            this.FamilleTextBox.Location = new System.Drawing.Point(209, 141);
            this.FamilleTextBox.Name = "FamilleTextBox";
            this.FamilleTextBox.ReadOnly = true;
            this.FamilleTextBox.Size = new System.Drawing.Size(326, 31);
            this.FamilleTextBox.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 25);
            this.label1.TabIndex = 21;
            this.label1.Text = "Reference";
            // 
            // ReferenceTextBox
            // 
            this.ReferenceTextBox.Location = new System.Drawing.Point(209, 77);
            this.ReferenceTextBox.Name = "ReferenceTextBox";
            this.ReferenceTextBox.ReadOnly = true;
            this.ReferenceTextBox.Size = new System.Drawing.Size(326, 31);
            this.ReferenceTextBox.TabIndex = 22;
            // 
            // FormModifySousFam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 362);
            this.Controls.Add(this.ReferenceTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FamilleTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Confirmer);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.NomTextBox);
            this.Controls.Add(this.label2);
            this.Name = "FormModifySousFam";
            this.Text = "Modifier une sous-famille";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Confirmer;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.TextBox NomTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox FamilleTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ReferenceTextBox;
    }
}