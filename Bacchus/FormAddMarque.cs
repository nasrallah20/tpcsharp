﻿using Bacchus.dao;
using System;
using System.Windows.Forms;

namespace Bacchus
{
    public partial class FormAddMarque : Form
    {
        public FormAddMarque()
        {
            InitializeComponent();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Confirmer_Click(object sender, EventArgs e)
        {
            if (NomTextBox.Text != "")
            {

                var Nom = NomTextBox.Text;

                Marque Marque = new Marque(-1, Nom);

                MarqueDao.Persist(Marque);

                ((FormMain)Owner).RefreshWindow();
                Close();

            }
            else
            {
                MessageBox.Show("Vous devez remplir tous les champs", "Erreur", MessageBoxButtons.OK);
            }
        }
    }
}
