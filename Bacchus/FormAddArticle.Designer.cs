﻿namespace Bacchus
{
    partial class FormAddArticle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ReferenceTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.FamilleCombobox = new System.Windows.Forms.ComboBox();
            this.SousFamilleCombobox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.RTextboxDescription = new System.Windows.Forms.RichTextBox();
            this.TextBoxQuantite = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TextBoxPrix = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ConfirmButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.MarqueCombobox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Reference :";
            // 
            // ReferenceTextBox
            // 
            this.ReferenceTextBox.Location = new System.Drawing.Point(217, 58);
            this.ReferenceTextBox.Name = "ReferenceTextBox";
            this.ReferenceTextBox.Size = new System.Drawing.Size(266, 31);
            this.ReferenceTextBox.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Famille :";
            // 
            // FamilleCombobox
            // 
            this.FamilleCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FamilleCombobox.FormattingEnabled = true;
            this.FamilleCombobox.Location = new System.Drawing.Point(217, 143);
            this.FamilleCombobox.Name = "FamilleCombobox";
            this.FamilleCombobox.Size = new System.Drawing.Size(266, 33);
            this.FamilleCombobox.TabIndex = 5;
            this.FamilleCombobox.SelectedIndexChanged += new System.EventHandler(this.FamilleCombobox_SelectedIndexChanged);
            // 
            // SousFamilleCombobox
            // 
            this.SousFamilleCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SousFamilleCombobox.FormattingEnabled = true;
            this.SousFamilleCombobox.Location = new System.Drawing.Point(217, 235);
            this.SousFamilleCombobox.Name = "SousFamilleCombobox";
            this.SousFamilleCombobox.Size = new System.Drawing.Size(266, 33);
            this.SousFamilleCombobox.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(59, 238);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "Sous-famille :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 458);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 25);
            this.label2.TabIndex = 8;
            this.label2.Text = "Description :";
            // 
            // RTextboxDescription
            // 
            this.RTextboxDescription.Location = new System.Drawing.Point(217, 416);
            this.RTextboxDescription.Name = "RTextboxDescription";
            this.RTextboxDescription.Size = new System.Drawing.Size(266, 138);
            this.RTextboxDescription.TabIndex = 9;
            this.RTextboxDescription.Text = "";
            // 
            // TextBoxQuantite
            // 
            this.TextBoxQuantite.Location = new System.Drawing.Point(217, 593);
            this.TextBoxQuantite.Name = "TextBoxQuantite";
            this.TextBoxQuantite.Size = new System.Drawing.Size(266, 31);
            this.TextBoxQuantite.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(59, 596);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 25);
            this.label5.TabIndex = 10;
            this.label5.Text = "Quantité :";
            // 
            // TextBoxPrix
            // 
            this.TextBoxPrix.Location = new System.Drawing.Point(217, 675);
            this.TextBoxPrix.Name = "TextBoxPrix";
            this.TextBoxPrix.Size = new System.Drawing.Size(266, 31);
            this.TextBoxPrix.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(59, 678);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 25);
            this.label6.TabIndex = 12;
            this.label6.Text = "Prix HT :";
            // 
            // ConfirmButton
            // 
            this.ConfirmButton.Location = new System.Drawing.Point(154, 768);
            this.ConfirmButton.Name = "ConfirmButton";
            this.ConfirmButton.Size = new System.Drawing.Size(123, 39);
            this.ConfirmButton.TabIndex = 14;
            this.ConfirmButton.Text = "Confirmer";
            this.ConfirmButton.UseVisualStyleBackColor = true;
            this.ConfirmButton.Click += new System.EventHandler(this.ConfirmButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(283, 768);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(109, 38);
            this.CancelButton.TabIndex = 15;
            this.CancelButton.Text = "Annuler";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // MarqueCombobox
            // 
            this.MarqueCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MarqueCombobox.FormattingEnabled = true;
            this.MarqueCombobox.Location = new System.Drawing.Point(217, 329);
            this.MarqueCombobox.Name = "MarqueCombobox";
            this.MarqueCombobox.Size = new System.Drawing.Size(266, 33);
            this.MarqueCombobox.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(59, 332);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 25);
            this.label7.TabIndex = 16;
            this.label7.Text = "Marque :";
            // 
            // FormAddArticle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 846);
            this.Controls.Add(this.MarqueCombobox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.ConfirmButton);
            this.Controls.Add(this.TextBoxPrix);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TextBoxQuantite);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.RTextboxDescription);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.SousFamilleCombobox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.FamilleCombobox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ReferenceTextBox);
            this.Controls.Add(this.label1);
            this.Name = "FormAddArticle";
            this.Text = "Ajouter un article";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ReferenceTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox FamilleCombobox;
        private System.Windows.Forms.ComboBox SousFamilleCombobox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox RTextboxDescription;
        private System.Windows.Forms.TextBox TextBoxQuantite;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TextBoxPrix;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button ConfirmButton;
        private new System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.ComboBox MarqueCombobox;
        private System.Windows.Forms.Label label7;
    }
}