﻿using Bacchus.dao;
using System.Windows.Forms;

namespace Bacchus
{
    public partial class FormModifyFamille : Form
    {
        public FormModifyFamille()
        {
            InitializeComponent();
        }

        public void GetSelectedElement(Famille Famille)
        {
            ReferenceTextBox.Text = Famille.RefFamille.ToString();
            NomTextBox.Text = Famille.Nom;
        }

        private void Confirmer_Click(object sender, System.EventArgs e)
        {
            if (NomTextBox.Text != "")
            {

                var Nom = NomTextBox.Text;

                Famille Famille = new Famille(int.Parse(ReferenceTextBox.Text), Nom);

                FamilleDao.UpdateFamille(Famille);

                ((FormMain)Owner).RefreshWindow();
                Close();

            }
            else
            {
                MessageBox.Show("Vous devez remplir tous les champs", "Erreur", MessageBoxButtons.OK);
            }
        }

        private void Cancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }

       
    }
}
