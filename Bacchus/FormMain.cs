﻿using Bacchus.dao;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections;

namespace Bacchus
{
    public partial class FormMain : Form
    {
        private string typeData;

        public FormMain()
        {
            InitializeComponent();
            CenterToScreen();
        }

        public void LoadTreeView()
        {

            TreeView.Nodes.Add("Articles", "Tous les articles");
            TreeView.Nodes.Add("Familles", "Familles");
            TreeView.Nodes.Add("Marques", "Marques");


            var Cpt = 0;

            List<Famille> AllFamilles = FamilleDao.GetAllFamilles();
            List<SousFamille> AllSousFamilles = SousFamilleDao.GetAllSousFamilles();
            List<Marque> AllMarques = MarqueDao.GetAllMarques();

            foreach(Famille Famille in AllFamilles)
            {
                TreeView.Nodes[1].Nodes.Add("Famille", Famille.Nom);

                foreach(SousFamille SousFamille in AllSousFamilles)
                {
                    if(SousFamille.Famille.Nom.Equals(Famille.Nom))
                    {
                        TreeView.Nodes[1].Nodes[Cpt].Nodes.Add("SousFamille", SousFamille.Nom);

                    }
                }

                Cpt++;
            }

            foreach (Marque Marque in AllMarques)
            {
                TreeView.Nodes[2].Nodes.Add("Marque", Marque.Nom);
            }

        }

        public void LoadListView()
        {
            var Node = TreeView.SelectedNode;

            if(Node!=null)
            {
                switch (Node.Name)
                {

                    case "Articles":

                        ListView1.Clear();
                        ListView1.Columns.Add("Description", 300);
                        ListView1.Columns.Add("Familles", 170);
                        ListView1.Columns.Add("SousFamilles", 200);
                        ListView1.Columns.Add("Marques", 100);
                        ListView1.Columns.Add("Quantite");
                        ListView1.Columns.Add("Prix H.T.");

                        List<Article> Articles = ArticleDao.GetAllArticles();
                        foreach (Article Article in Articles)
                        {

                            string[] ArticleData = {Article.Description, Article.SousFamille.Famille.Nom,
                            Article.SousFamille.Nom, Article.Marque.Nom, Convert.ToString(Article.Quantite),
                            Convert.ToString(Article.PrixHT), Article.RefArticle};

                            ListViewItem Item = new ListViewItem(ArticleData);
                            ListView1.Items.Add(Item);
                        }

                        typeData = "Articles";

                        break;

                    case "Familles":

                        ListView1.Clear();
                        ListView1.Columns.Add("Description", 300);

                        List<Famille> Familles = FamilleDao.GetAllFamilles();
                        foreach (Famille Fam in Familles)
                        {
                            String[] Famille = { Fam.Nom, Fam.RefFamille.ToString() };
                            ListViewItem Item = new ListViewItem(Famille);
                            ListView1.Items.Add(Item);
                        }

                        typeData = "Familles";
                        break;
                        

                    case "Famille":

                        ListView1.Clear();
                        ListView1.Columns.Add("Description", 300);

                        Famille FamilleNode = FamilleDao.GetFamilleByNom(Node.Text);

                        List<SousFamille> SousFamilles = SousFamilleDao.GetSousFamilleByFamille(FamilleNode);
                        foreach (SousFamille SousFam in SousFamilles)
                        {
                            String[] SousFamille = { SousFam.Nom, SousFam.Reference.ToString() };
                            ListViewItem Item = new ListViewItem(SousFamille);
                            ListView1.Items.Add(Item);
                        }

                        typeData = "Famille";
                        break;

                    case "SousFamille":

                        ListView1.Clear();
                        ListView1.Columns.Add("Description", 300);
                        ListView1.Columns.Add("Familles", 170);
                        ListView1.Columns.Add("SousFamilles", 200);
                        ListView1.Columns.Add("Marques", 100);
                        ListView1.Columns.Add("Quantite");
                        ListView1.Columns.Add("Prix H.T.");

                        SousFamille SousFamilleNode = SousFamilleDao.GetSousFamilleByNom(Node.Text);

                        List<Article> ArticlesBySousFam = ArticleDao.GetAllArticlesBySousFamille(SousFamilleNode.Reference.ToString());
                        foreach (Article Article in ArticlesBySousFam)
                        {
                            string[] Art = {Article.Description, Article.SousFamille.Famille.Nom,
                            Article.SousFamille.Nom, Article.Marque.Nom, Convert.ToString(Article.Quantite),
                            Convert.ToString(Article.PrixHT), Article.RefArticle};
                            ListViewItem Item = new ListViewItem(Art);
                            ListView1.Items.Add(Item);
                        }

                        typeData = "SousFamille";
                        break;

                    case "Marques":

                        ListView1.Clear();
                        ListView1.Columns.Add("Description", 300);

                        List<Marque> Marques = MarqueDao.GetAllMarques();
                        foreach (Marque Marque in Marques)
                        {
                            string[] Mar = { Marque.Nom , Marque.RefMarque.ToString()};
                            ListViewItem Item = new ListViewItem(Mar);
                            ListView1.Items.Add(Item);
                        }

                        typeData = "Marques";
                        break;


                    case "Marque" :

                        ListView1.Clear();
                        ListView1.Columns.Add("Description", 300);
                        ListView1.Columns.Add("Familles", 170);
                        ListView1.Columns.Add("SousFamilles", 200);
                        ListView1.Columns.Add("Marques", 100);
                        ListView1.Columns.Add("Quantite");
                        ListView1.Columns.Add("Prix H.T.");

                        Marque MarqueNode = MarqueDao.GetMarqueByNom(Node.Text);

                        List<Article> ArticlesList = ArticleDao.GetAllArticlesByMarque(MarqueNode.RefMarque);
                        foreach (Article Article in ArticlesList)
                        {
                    
                            string[] ArticleData = {Article.Description, Article.SousFamille.Famille.Nom,
                            Article.SousFamille.Nom, Article.Marque.Nom, Convert.ToString(Article.Quantite),
                            Convert.ToString(Article.PrixHT)};

                            ListViewItem Item = new ListViewItem(ArticleData);
                            ListView1.Items.Add(Item);
                        }

                        typeData = "Marque";
                        break;

                }
            }
        }

        private void TreeView1_AfterSelect(System.Object sender, System.Windows.Forms.TreeViewEventArgs e)
        {
            LoadListView();
            InformationStatusStrip();
        }

        private void ImporterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormImport myForm = new FormImport();
            
            myForm.ShowDialog(this);

        }

        private void ExporterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form ExportForm = new FormExport();

            ExportForm.ShowDialog();

        }

        private void ActualiserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RefreshWindow();
        }

        private void ListView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
          
            this.lvwColumnSorter.SortColumn = e.Column;
         
            // Perform the sort with these new sort options.
            this.ListView1.Sort();
            if (this.lvwColumnSorter.SortOrder == true)
            {
                this.lvwColumnSorter.SortOrder = false;
            }
            else
            {
                this.lvwColumnSorter.SortOrder = true;
            }
        }

      

        public void listView1_KeyUp(object Sender, KeyEventArgs Event)
        {
            switch (Event.KeyCode)
            {
                case Keys.Delete:
                    DeleteElement();
                    break;
                case Keys.F5:
                    RefreshWindow();
                    break;
                case Keys.Enter:
                    ModifyEelement();
                    break;
            }
        }
        
        public void InformationStatusStrip()
        {
            switch (typeData)
            {
                case "Articles":
                    toolStripStatusLabel1.Text = ArticleDao.GetAllArticles().Count + " articles";
                    break;

                case "Familles":
                    toolStripStatusLabel1.Text = FamilleDao.GetAllFamilles().Count + " familles";
                    break;

                case "Marques":
                    toolStripStatusLabel1.Text = MarqueDao.GetAllMarques().Count +  " marques";
                    break;

                case "Famille":
                    toolStripStatusLabel1.Text = SousFamilleDao.GetAllSousFamilles().Count + " sous-famil";
                    break;
            }
        }

        
        public void RefreshWindow()
        {
    
            TreeView.Nodes.Clear();
            ListView1.Clear();
            LoadTreeView();
            LoadListView();
            InformationStatusStrip();
        }

        public void DeleteElement()
        {
            switch (typeData)
            {
                case "Articles":
                    var Article = ListView1.SelectedItems[0].SubItems[6].Text;
                    DialogResult DialogResult = MessageBox.Show("Êtes-vous sûrs de vouloir suprrimer l'article ?", "Warning", MessageBoxButtons.YesNo);
                    if (DialogResult == DialogResult.Yes)
                    {
                        ArticleDao.DeleteArticleByReference(Article);

                    }
                    break;

                case "Familles":
                    var Famille = ListView1.SelectedItems[0].SubItems[1].Text;
                    if(SousFamilleDao.GetSousFamilleByFamille(FamilleDao.GetFamilleByReference(long.Parse(Famille))).Count == 0)
                    {
                        DialogResult DialogResult1 = MessageBox.Show("Êtes-vous sûrs de vouloir suprrimer la famille ?", "Warning", MessageBoxButtons.YesNo);
                        if(DialogResult1 == DialogResult.Yes)
                        {
                            FamilleDao.DeleteFamilleByReference(long.Parse(Famille));

                        }
                    }
                    else
                    {
                       MessageBox.Show("Vous devez supprimer les sous-familles reliées à cette famille d'abord", "Warning", MessageBoxButtons.OK);
                    }
                    break;

                case "Marques":
                    var Marque = ListView1.SelectedItems[0].SubItems[1].Text;

                    if(ArticleDao.GetAllArticlesByMarque(long.Parse(Marque)).Count == 0)
                    {
                        DialogResult DialogResult2 = MessageBox.Show("Êtes-vous sûrs de vouloir suprrimer la marque ?", "Warning", MessageBoxButtons.YesNo);
                        if (DialogResult2 == DialogResult.Yes)
                        {
                            MarqueDao.DeleteMarqueByReference(long.Parse(Marque));

                        }
                    }
                    else
                    {
                        MessageBox.Show("Vous devez supprimer les articles reliées à cette marque d'abord", "Warning", MessageBoxButtons.OK);
                    }
                    break;

                case "Famille":
                    var SousFamille = ListView1.SelectedItems[0].SubItems[1].Text;

                    if(ArticleDao.GetAllArticlesBySousFamille(SousFamille).Count == 0)
                    {
                        DialogResult DialogResult3 = MessageBox.Show("Êtes-vous sûrs de vouloir suprrimer la sous-famille ?", "Warning", MessageBoxButtons.YesNo);
                        if (DialogResult3 == DialogResult.Yes)
                        {
                            SousFamilleDao.DeleteSousFamilleByReference(long.Parse(SousFamille));

                        }
                    }
                    else
                    {
                        MessageBox.Show("Vous devez supprimer les articles reliées à cette marque d'abord", "Warning", MessageBoxButtons.OK);
                    }
                    break;
            }
        }

        private void ListView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (ListView1.FocusedItem.Bounds.Contains(e.Location))
                {
                    ContextMenuListView.Show(Cursor.Position);
                }
            }
        }

        private void AddEelement()
        {
            switch (typeData)
            {
                case "Articles":
                    FormAddArticle AddArticleForm = new FormAddArticle();
                    AddArticleForm.StartPosition = FormStartPosition.CenterParent;
                    AddArticleForm.ShowDialog(this);
                    break;

                case "Familles":
                    FormAddFamille FormAddFamille = new FormAddFamille();
                    FormAddFamille.StartPosition = FormStartPosition.CenterParent;
                    FormAddFamille.ShowDialog(this);
                    break;

                case "Marques":
                    FormAddMarque FormAddMarque = new FormAddMarque();
                    FormAddMarque.StartPosition = FormStartPosition.CenterParent;
                    FormAddMarque.ShowDialog(this);
                    break;

                case "Famille":
                    FormAddSousFamille FormAddSousFamille = new FormAddSousFamille();
                    FormAddSousFamille.StartPosition = FormStartPosition.CenterParent;
                    FormAddSousFamille.ShowDialog(this);
                    break;
            }
        }

        public void ModifyEelement()
        {
            var Item = ListView1.SelectedItems[0];

            switch (typeData)
            {
                case "Articles":
                    FormModifyArticle ModifyArticleForm = new FormModifyArticle();
                    ModifyArticleForm.StartPosition = FormStartPosition.CenterParent;
                    Article Article = ArticleDao.GetArticleByReference(Item.SubItems[6].Text);
                    ModifyArticleForm.GetSelectedElement(Article);
                    ModifyArticleForm.ShowDialog(this);
                    break;

                case "Familles":
                    FormModifyFamille ModifyFamilleForm = new FormModifyFamille();
                    ModifyFamilleForm.StartPosition = FormStartPosition.CenterParent;
                    Famille Famille = FamilleDao.GetFamilleByReference(long.Parse(Item.SubItems[1].Text));
                    ModifyFamilleForm.GetSelectedElement(Famille);
                    ModifyFamilleForm.ShowDialog(this);
                    break;

                case "Marques":
                    FormModifyMarque ModifyMarqueForm = new FormModifyMarque();
                    ModifyMarqueForm.StartPosition = FormStartPosition.CenterParent;
                    Marque Marque = MarqueDao.GetMarqueByReference(long.Parse(Item.SubItems[1].Text));
                    ModifyMarqueForm.GetSelectedElement(Marque);
                    ModifyMarqueForm.ShowDialog(this);
                    break;

                case "Famille":
                    FormModifySousFam ModifySousFamForm = new FormModifySousFam();
                    ModifySousFamForm.StartPosition = FormStartPosition.CenterParent;
                    SousFamille SousFamille = SousFamilleDao.GetSousFamilleByReference(long.Parse(Item.SubItems[1].Text));
                    ModifySousFamForm.GetSelectedElement(SousFamille);
                    ModifySousFamForm.ShowDialog(this);
                    break;
            }
        }

        private void AjouterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddEelement();
            LoadListView();
            InformationStatusStrip();
        }

        private void SupprmerContextMenuStrip_Click(object sender, EventArgs e)
        {
            DeleteElement();
            LoadListView();
            InformationStatusStrip();
        }

        private void ModiferToolStrip_Click(object sender, EventArgs e)
        {
            ModifyEelement();
            LoadListView();
            InformationStatusStrip();
        }

    } 
}
