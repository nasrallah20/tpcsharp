﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Bacchus
{
    public partial class FormImport : Form
    {
        public FormImport()
        {
            InitializeComponent();
        }

        private void ButtonEcr_Click(object sender, EventArgs e)
        {
            try
            {
                int Numlines = gestionFichier.Import.ImportCSV(textBox1.Text, true, progressBar1);
                
                ((FormMain)Owner).RefreshWindow();

                MessageBox.Show("Importation réussie. Ajout de " + Numlines + " articles");

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error is " + ex.ToString());
                throw;
            }

        }

        private void ButtonAjout_Click(object sender, EventArgs e)
        {
            try
            {
                int Numlines = gestionFichier.Import.ImportCSV(textBox1.Text, false, progressBar1);

                ((FormMain)Owner).RefreshWindow();



                MessageBox.Show("Importation réussie. Ajout de " + Numlines + " articles");

                Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error is " + ex.ToString());
                throw;
            }

        }

        private void ButtonSelectFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Multiselect = true;
            fileDialog.Title = "Sélectionner un fichier csv";
            fileDialog.Filter = "CSV Files (*.csv)|*.csv";
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                Stream stream = fileDialog.OpenFile();
                string path = fileDialog.FileName.ToString();
                textBox1.Text = path;
                stream.Close();
            }

        }
    }
}
