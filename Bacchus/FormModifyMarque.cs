﻿using Bacchus.dao;
using System.Windows.Forms;

namespace Bacchus
{
    public partial class FormModifyMarque : Form
    {
        public FormModifyMarque()
        {
            InitializeComponent();
        }

        public void GetSelectedElement(Marque Marque)
        {
            ReferenceTextBox.Text = Marque.RefMarque.ToString();
            NomTextBox.Text = Marque.Nom;
        }

        private void Cancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void Confirmer_Click(object sender, System.EventArgs e)
        {
            if (NomTextBox.Text != "")
            {

                var Nom = NomTextBox.Text;

                Marque Marque = new Marque(long.Parse(ReferenceTextBox.Text), Nom);

                MarqueDao.UpdateMarque(Marque);

                ((FormMain)Owner).RefreshWindow();
                Close();

            }
            else
            {
                MessageBox.Show("Vous devez remplir tous les champs", "Erreur", MessageBoxButtons.OK);
            }
        }
    }
}
