﻿namespace Bacchus
{
    partial class FormAddSousFamille
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Confirmer = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.NomTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.FamilleCombobox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Confirmer
            // 
            this.Confirmer.Location = new System.Drawing.Point(343, 200);
            this.Confirmer.Name = "Confirmer";
            this.Confirmer.Size = new System.Drawing.Size(104, 40);
            this.Confirmer.TabIndex = 11;
            this.Confirmer.Text = "Confirmer";
            this.Confirmer.UseVisualStyleBackColor = true;
            this.Confirmer.Click += new System.EventHandler(this.Confirmer_Click);
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(218, 200);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(104, 40);
            this.Cancel.TabIndex = 10;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // NomTextBox
            // 
            this.NomTextBox.Location = new System.Drawing.Point(203, 114);
            this.NomTextBox.Name = "NomTextBox";
            this.NomTextBox.Size = new System.Drawing.Size(244, 31);
            this.NomTextBox.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 25);
            this.label2.TabIndex = 7;
            this.label2.Text = "Nom";
            // 
            // FamilleCombobox
            // 
            this.FamilleCombobox.FormattingEnabled = true;
            this.FamilleCombobox.Location = new System.Drawing.Point(203, 54);
            this.FamilleCombobox.Name = "FamilleCombobox";
            this.FamilleCombobox.Size = new System.Drawing.Size(244, 33);
            this.FamilleCombobox.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(66, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 25);
            this.label3.TabIndex = 13;
            this.label3.Text = "Famille";
            // 
            // FormAddSousFamille
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 264);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.FamilleCombobox);
            this.Controls.Add(this.Confirmer);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.NomTextBox);
            this.Controls.Add(this.label2);
            this.Name = "FormAddSousFamille";
            this.Text = "Ajouter une sous-famille";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Confirmer;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.TextBox NomTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox FamilleCombobox;
        private System.Windows.Forms.Label label3;
    }
}