﻿using Bacchus.dao;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bacchus
{
    public partial class FormModifySousFam : Form
    {
        public FormModifySousFam()
        {
            InitializeComponent();
        }

        public void GetSelectedElement(SousFamille SousFamille)
        {
            ReferenceTextBox.Text = SousFamille.Reference.ToString();
            FamilleTextBox.Text = SousFamille.Famille.Nom;
            NomTextBox.Text = SousFamille.Nom;
        }

        private void Confirmer_Click(object sender, EventArgs e)
        {
            if (NomTextBox.Text != "")
            {
                var Nom = NomTextBox.Text;

                Famille Famille = FamilleDao.GetFamilleByNom(FamilleTextBox.Text);

                SousFamille SousFamille = new SousFamille(long.Parse(ReferenceTextBox.Text)
                    , Famille , Nom);

                SousFamilleDao.UpdateSousFamille(SousFamille);

                ((FormMain)Owner).RefreshWindow();
                Close();

            }
            else
            {
                MessageBox.Show("Vous devez remplir tous les champs", "Erreur", MessageBoxButtons.OK);
            }
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
