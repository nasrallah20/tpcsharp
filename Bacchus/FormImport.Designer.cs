﻿namespace Bacchus
{
    partial class FormImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ButtonSelectFile = new System.Windows.Forms.Button();
            this.ButtonEcr = new System.Windows.Forms.Button();
            this.ButtonAjout = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ButtonSelectFile
            // 
            this.ButtonSelectFile.Location = new System.Drawing.Point(370, 68);
            this.ButtonSelectFile.Margin = new System.Windows.Forms.Padding(6);
            this.ButtonSelectFile.Name = "ButtonSelectFile";
            this.ButtonSelectFile.Size = new System.Drawing.Size(150, 48);
            this.ButtonSelectFile.TabIndex = 0;
            this.ButtonSelectFile.Text = "Select";
            this.ButtonSelectFile.UseVisualStyleBackColor = true;
            this.ButtonSelectFile.Click += new System.EventHandler(this.ButtonSelectFile_Click);
            // 
            // ButtonEcr
            // 
            this.ButtonEcr.Location = new System.Drawing.Point(45, 187);
            this.ButtonEcr.Margin = new System.Windows.Forms.Padding(6);
            this.ButtonEcr.Name = "ButtonEcr";
            this.ButtonEcr.Size = new System.Drawing.Size(240, 48);
            this.ButtonEcr.TabIndex = 1;
            this.ButtonEcr.Text = "Mode ecrasement";
            this.ButtonEcr.UseVisualStyleBackColor = true;
            this.ButtonEcr.Click += new System.EventHandler(this.ButtonEcr_Click);
            // 
            // ButtonAjout
            // 
            this.ButtonAjout.Location = new System.Drawing.Point(45, 263);
            this.ButtonAjout.Margin = new System.Windows.Forms.Padding(6);
            this.ButtonAjout.Name = "ButtonAjout";
            this.ButtonAjout.Size = new System.Drawing.Size(240, 48);
            this.ButtonAjout.TabIndex = 2;
            this.ButtonAjout.Text = "Mode ajout";
            this.ButtonAjout.UseVisualStyleBackColor = true;
            this.ButtonAjout.Click += new System.EventHandler(this.ButtonAjout_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(45, 382);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(6);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(496, 48);
            this.progressBar1.TabIndex = 4;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(45, 77);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(292, 31);
            this.textBox1.TabIndex = 5;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(206, 25);
            this.label2.TabIndex = 7;
            this.label2.Text = "Mode d\'importation :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(217, 25);
            this.label3.TabIndex = 8;
            this.label3.Text = "Choisir le fichier csv :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 342);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(139, 25);
            this.label4.TabIndex = 9;
            this.label4.Text = "Progression :";
            // 
            // FormImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(566, 458);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.ButtonAjout);
            this.Controls.Add(this.ButtonEcr);
            this.Controls.Add(this.ButtonSelectFile);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "FormImport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Importer les données";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonSelectFile;
        private System.Windows.Forms.Button ButtonEcr;
        private System.Windows.Forms.Button ButtonAjout;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}