﻿using Bacchus.dao;
using System;
using System.Windows.Forms;

namespace Bacchus
{
    public partial class FormAddFamille : Form
    {
        public FormAddFamille()
        {
            InitializeComponent();
        }

        private void Confirmer_Click(object sender, EventArgs e)
        {
            if (NomTextBox.Text != "")
            {

                var Nom = NomTextBox.Text;

                Famille Famille = new Famille(-1, Nom);

                FamilleDao.Persist(Famille);

                ((FormMain)Owner).RefreshWindow();
                Close();

            }
            else
            {
                MessageBox.Show("Vous devez remplir tous les champs", "Erreur", MessageBoxButtons.OK);
            }
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

      
    }
}
