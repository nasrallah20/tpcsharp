﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;


namespace Bacchus.dao
{
    class FamilleDao : Dao
    {
        public static long Persist(Famille Fam)
        {
            long RefFamille = -1;
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT RefFamille FROM Familles WHERE Nom=@Nom";
                Command.Parameters.Add(new SQLiteParameter("@Nom", Fam.Nom));
                SQLiteDataReader QqReader = Command.ExecuteReader();

                if (QqReader.Read())//Already exite get refFamille
                {
                    RefFamille = QqReader.GetInt64(0);
                    QqReader.Close();
                }
                else
                {
                    QqReader.Close();
                    Command.CommandText = "INSERT INTO Familles (Nom) VALUES( @Nom) ";

                    Command.CommandType = System.Data.CommandType.Text;

                    Command.Parameters.Add(new SQLiteParameter("@Nom", Fam.Nom));

                    int Status = Command.ExecuteNonQuery();

                    RefFamille = SqliteConn.LastInsertRowId;

                }
            }
            return RefFamille;
        }

        public static  void UpdateFamille(Famille Famille)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "UPDATE Familles SET Nom = @Nom WHERE RefFamille = @RefFamille ";
                Command.CommandType = System.Data.CommandType.Text;

                Command.Parameters.Add(new SQLiteParameter("@RefFamille", Famille.RefFamille));
                Command.Parameters.Add(new SQLiteParameter("@Nom", Famille.Nom));
                int Status = Command.ExecuteNonQuery();

            }
        }

        public static string GetNomFamilleByReference(long Reference)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT Nom FROM Familles WHERE RefFamille = @Reference";
                Command.Parameters.Add(new SQLiteParameter("@Reference", Reference));
                Command.ExecuteNonQuery();

                var Reader = Command.ExecuteReader();

                String NomFamille = "";
                while (Reader.Read())
                {
                    NomFamille = Reader.GetString(1);
                }

                SqliteConn.Close();

                return NomFamille;
            }
        }

        public static List<Famille> GetAllFamilles()
        {
            List<Famille> Familles = new List<Famille>();
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT * FROM Familles";
                Command.ExecuteNonQuery();

                var Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    var Reference = Reader.GetInt16(0);
                    var Nom = Reader.GetString(1);

                    Famille Famille = new Famille(Reference, Nom);

                    Familles.Add(Famille);
                }

                SqliteConn.Close();
                return Familles;
            }
        }

        public static Famille GetFamilleByReference(long Reference)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT * FROM Familles WHERE RefFamille = @Reference";
                Command.Parameters.Add(new SQLiteParameter("@Reference", Reference));
                Command.ExecuteNonQuery();

                var Reader = Command.ExecuteReader();

                Famille Famille = new Famille();
                while (Reader.Read())
                {
                    var RefFamille = Reader.GetInt32(0);
                    var NomFamille = Reader.GetString(1);
                    Famille.RefFamille = RefFamille;
                    Famille.Nom = NomFamille;
                }

                SqliteConn.Close();

                return Famille;
            }

        }

        public static Famille GetFamilleByNom(String Nom)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT * FROM Familles WHERE Nom = @Nom";
                Command.Parameters.Add(new SQLiteParameter("@Nom", Nom));
                Command.ExecuteNonQuery();

                var Reader = Command.ExecuteReader();

                Famille Famille = new Famille();
                while (Reader.Read())
                {
                    var RefFamille = Reader.GetInt32(0);
                    var NomFamille = Reader.GetString(1);
                    Famille.RefFamille = RefFamille;
                    Famille.Nom = NomFamille;
                }

                SqliteConn.Close();

                return Famille;
            }

        }
        public static List<String> GetNomFamilles()
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT Nom FROM Familles";
                Command.ExecuteNonQuery();
                List<String> NomFamilles = new List<string>();

                var Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    var nom = Reader.GetString(0);
                    NomFamilles.Add(nom);
                }

                SqliteConn.Close();
                return NomFamilles;
            }
        }

        public static void DeleteFamilles()
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "DELETE FROM Familles";
                Command.ExecuteNonQuery();

                SqliteConn.Close();
            }
        }

        public static void DeleteFamilleByReference(long Reffamille)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "DELETE FROM Familles WHERE RefFamille = @Reference";
                Command.Parameters.Add(new SQLiteParameter("@Reference", Reffamille));
                Command.ExecuteNonQuery();

                SqliteConn.Close();
            }
        }
    }
}
