﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bacchus.dao
{
    class MarqueDao:Dao
    {
        public static long Persist(Marque Mar)
        {
            long RefMarque = -1;
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT RefMarque FROM Marques WHERE Nom=@Nom";
                Command.Parameters.Add(new SQLiteParameter("@Nom", Mar.Nom));

                SQLiteDataReader SqReader = Command.ExecuteReader();

                if (SqReader.Read())//Already exite reset refMarque
                {
                    RefMarque = SqReader.GetInt64(0);
                    SqReader.Close();
                }
                else
                {
                    SqReader.Close();
                    Command.CommandText = "INSERT INTO Marques (Nom) VALUES( @Nom) ";

                    Command.CommandType = System.Data.CommandType.Text;

                    Command.Parameters.Add(new SQLiteParameter("@Nom", Mar.Nom));

                    int Status = Command.ExecuteNonQuery();

                    RefMarque = SqliteConn.LastInsertRowId;
                }
            }
            return RefMarque;
        }

        public static void UpdateMarque(Marque Marque)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "UPDATE Marques SET Nom = @Nom WHERE RefMarque = @RefMarque ";
                Command.CommandType = System.Data.CommandType.Text;

                Command.Parameters.Add(new SQLiteParameter("@RefMarque", Marque.RefMarque));
                Command.Parameters.Add(new SQLiteParameter("@Nom", Marque.Nom));
                int Status = Command.ExecuteNonQuery();

            }
        }

        public static Marque GetMarqueByReference(long Reference)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT * FROM Marques WHERE RefMarque = @Reference";
                Command.Parameters.Add(new SQLiteParameter("@Reference", Reference));
                Command.ExecuteNonQuery();

                var Reader = Command.ExecuteReader();

                Marque Marque = new Marque();
             
                while(Reader.Read())
                {
                    var ReferenceMarque = Reader.GetInt32(0);
                    var NomMarque = Reader.GetString(1);

                    Marque.RefMarque = ReferenceMarque;
                    Marque.Nom = NomMarque;
                }

                SqliteConn.Close();

                return Marque;
            }
        }

        public static Marque GetMarqueByNom(string Nom)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT * FROM Marques WHERE Nom = @Nom";
                Command.Parameters.Add(new SQLiteParameter("@Nom", Nom));
                Command.ExecuteNonQuery();

                var Reader = Command.ExecuteReader();

                Marque Marque = new Marque();

                while (Reader.Read())
                {
                    var ReferenceMarque = Reader.GetInt32(0);
                    var NomMarque = Reader.GetString(1);

                    Marque.RefMarque = ReferenceMarque;
                    Marque.Nom = NomMarque;
                }

                SqliteConn.Close();

                return Marque;
            }
        }





        public static List<Marque> GetAllMarques()
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                List<Marque> Marques = new List<Marque>();
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT * FROM Marques";
                Command.ExecuteNonQuery();

                var Reader = Command.ExecuteReader();

                while (Reader.Read())
                {
                    var ReferenceMarque = Reader.GetInt32(0);
                    var NomMarque = Reader.GetString(1);

                    Marque Marque = new Marque(ReferenceMarque, NomMarque);
                    Marques.Add(Marque);
                }

                SqliteConn.Close();

                return Marques;
            }
        }

        public static void DeleteMarques()
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "DELETE FROM Marques";
                Command.ExecuteNonQuery();

                SqliteConn.Close();
            }
        }

        public static void DeleteMarqueByReference(long RefMarque)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "DELETE FROM Marques WHERE RefMarque = @RefMarque";
                Command.Parameters.Add(new SQLiteParameter("@RefMarque", RefMarque));

                Command.ExecuteNonQuery();

                SqliteConn.Close();
            }
        }
    }

}
