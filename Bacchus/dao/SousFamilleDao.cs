﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace Bacchus.dao
{
    class SousFamilleDao : Dao
    {
        public static long Persist(SousFamille SousFam)
        {
            long RefSousFamille = -1;
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT RefSousFamille,RefFamille FROM SousFamilles WHERE Nom=@Nom";
                Command.Parameters.Add(new SQLiteParameter("@Nom", SousFam.Nom));
                SQLiteDataReader SqReader = Command.ExecuteReader();

                if (SqReader.Read())//Already exite SousFamille
                {
                    RefSousFamille = SqReader.GetInt64(0);
                    SousFam.Reference =  SqReader.GetInt64(0);

                    SqReader.Close();
                }
                else
                {
                    SqReader.Close();
                    Command.CommandText = "INSERT INTO SousFamilles (RefFamille,Nom) VALUES( @refFamille,@Nom) ";

                    Command.CommandType = System.Data.CommandType.Text;

                    Command.Parameters.Add(new SQLiteParameter("@Nom", SousFam.Nom));
                    Command.Parameters.Add(new SQLiteParameter("@refFamille", SousFam.Famille.RefFamille));
                    int Status = Command.ExecuteNonQuery();

                    RefSousFamille = SqliteConn.LastInsertRowId;

                }
            }

            return RefSousFamille;
        }

        public static void UpdateSousFamille(SousFamille SousFamille)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "UPDATE SousFamilles SET Nom = @Nom WHERE RefSousFamille = @RefSousFamille ";
                Command.CommandType = System.Data.CommandType.Text;

                Command.Parameters.Add(new SQLiteParameter("@RefSousFamille", SousFamille.Reference));
                Command.Parameters.Add(new SQLiteParameter("@Nom", SousFamille.Nom));
                int Status = Command.ExecuteNonQuery();

            }
        }

        public static SousFamille GetSousFamilleByReference(long Reference)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT * FROM SousFamilles WHERE RefSousFamille = @Reference";
                Command.Parameters.Add(new SQLiteParameter("@Reference", Reference));
                Command.ExecuteNonQuery();

                var Reader = Command.ExecuteReader();

                var ReferenceSousFamille = 0;
                var ReferenceFamille = 0;
                String NomSousFamille = "";
                SousFamille SousFamille = new SousFamille();
                Famille Famille = new Famille();

                while (Reader.Read())
                {
                    ReferenceSousFamille = Reader.GetInt32(0);
                    ReferenceFamille = Reader.GetInt32(1);
                    NomSousFamille = Reader.GetString(2);

                    Famille = FamilleDao.GetFamilleByReference(ReferenceFamille);

                    SousFamille.Reference = ReferenceSousFamille;
                    SousFamille.Famille = Famille;
                    SousFamille.Nom = NomSousFamille;
                }

                SqliteConn.Close();

                return SousFamille;
            }
            
        }

        public static SousFamille GetSousFamilleByNom(string Nom)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT * FROM SousFamilles WHERE Nom = @NomSF";
                Command.Parameters.Add(new SQLiteParameter("@NomSF", Nom));
                Command.ExecuteNonQuery();

                var Reader = Command.ExecuteReader();

                var ReferenceSousFamille = 0;
                var ReferenceFamille = 0;
                String NomSousFamille = "";
                SousFamille SousFamille = new SousFamille();
                Famille Famille = new Famille();

                while (Reader.Read())
                {
                    ReferenceSousFamille = Reader.GetInt32(0);
                    ReferenceFamille = Reader.GetInt32(1);
                    NomSousFamille = Reader.GetString(2);

                    Famille = FamilleDao.GetFamilleByReference(ReferenceFamille);

                    SousFamille.Reference = ReferenceSousFamille;
                    SousFamille.Famille = Famille;
                    SousFamille.Nom = NomSousFamille;
                }

                SqliteConn.Close();

                return SousFamille;
            }

        }

        public static Famille GetFamilleByReference(long Reference)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT RefFamille FROM SousFamilles WHERE RefSousFamille = @Reference";
                Command.Parameters.Add(new SQLiteParameter("@Reference", Reference));
                Command.ExecuteNonQuery();

                var Reader = Command.ExecuteReader();

                int RefFamille = 0;
                while (Reader.Read())
                {
                    RefFamille = Reader.GetInt32(1);
                }

                SqliteConn.Close();

                return FamilleDao.GetFamilleByReference(RefFamille);
            }
        }

        public static List<SousFamille> GetAllSousFamilles()
        {
            List<SousFamille> SousFamilles = new List<SousFamille>();
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT * FROM SousFamilles";
                Command.ExecuteNonQuery();

                var Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    var Reference = Reader.GetInt16(0);
                    var RefFamille = Reader.GetInt16(1);
                    Famille Famille = FamilleDao.GetFamilleByReference(RefFamille);
                    var Nom = Reader.GetString(2);

                    SousFamille SousFamille = new SousFamille(Reference, Famille, Nom);

                    SousFamilles.Add(SousFamille);
                }

                SqliteConn.Close();
                return SousFamilles;
            }
        }

        public static List<SousFamille> GetSousFamilleByFamille(Famille Famille)
        {
            List<SousFamille> SousFamilleByFamille = new List<SousFamille>();
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT RefSousFamille, Nom FROM SousFamilles WHERE RefFamille = @Reference";
                Command.Parameters.Add(new SQLiteParameter("@Reference", Famille.RefFamille));
                Command.ExecuteNonQuery();

                var Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    var Reference = Reader.GetInt32(0);
                    var Nom = Reader.GetString(1);

                    var SousFam = new SousFamille(Reference, Famille, Nom);
                    SousFamilleByFamille.Add(SousFam);
                }

                SqliteConn.Close();
                return SousFamilleByFamille;
            }
        }


        public static void DeleteSousFamille()
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "DELETE FROM SousFamilles";
                Command.ExecuteNonQuery();

                SqliteConn.Close();
            }
        }

        public static void DeleteSousFamilleByReference(long Reference)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "DELETE FROM SousFamilles WHERE RefSousFamille = @RefSousFamille";
                Command.Parameters.Add(new SQLiteParameter("@RefSousFamille", Reference));
                Command.ExecuteNonQuery();

                SqliteConn.Close();
            }
        }
    }
}
