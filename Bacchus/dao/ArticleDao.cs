﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace Bacchus.dao
{
    class ArticleDao : Dao
    {
        public static void Persist(Article Art)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT count(*) FROM Articles WHERE RefArticle=@Ref";
                Command.Parameters.Add(new SQLiteParameter("@Ref", Art.RefArticle));

                long Count = Convert.ToInt64(Command.ExecuteScalar());
                if (Count == 0)
                {
                    Command.CommandText = "INSERT INTO Articles (RefArticle,Description,RefSousFamille,RefMarque,PrixHT,Quantite) VALUES( @RefArticle,@Description,@RefSousFamille,@RefMarque,@PrixHT,@Quantite) ";

                }
                else // article exist already
                {
                    Command.CommandText = "UPDATE Articles SET Description =@Description ," +
                        "RefSousFamille =@RefSousFamille ," +
                        "RefMarque = @RefMarque ," +
                        "PrixHT = @PrixHT," +
                        "Quantite=@Quantite WHERE RefArticle=@RefArticle ";
                }
                Command.CommandType = System.Data.CommandType.Text;

                Command.Parameters.Add(new SQLiteParameter("@RefArticle", Art.RefArticle));
                Command.Parameters.Add(new SQLiteParameter("@Description", Art.Description));
                Command.Parameters.Add(new SQLiteParameter("@RefSousFamille", Art.SousFamille.Reference));
                Command.Parameters.Add(new SQLiteParameter("@RefMarque", Art.Marque.RefMarque));
                Command.Parameters.Add(new SQLiteParameter("@PrixHT", Art.PrixHT));
                Command.Parameters.Add(new SQLiteParameter("@Quantite", Art.Quantite));
                int Status = Command.ExecuteNonQuery();

            }
        }

        public static void UpdateArticle(Article Article)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "UPDATE Articles SET Description =@Description ," +
                       "RefSousFamille =@RefSousFamille ," +
                       "RefMarque = @RefMarque ," +
                       "PrixHT = @PrixHT," +
                       "Quantite=@Quantite WHERE RefArticle=@RefArticle ";
                Command.CommandType = System.Data.CommandType.Text;

                Command.Parameters.Add(new SQLiteParameter("@RefArticle", Article.RefArticle));
                Command.Parameters.Add(new SQLiteParameter("@Description", Article.Description));
                Command.Parameters.Add(new SQLiteParameter("@RefSousFamille", Article.SousFamille.Reference));
                Command.Parameters.Add(new SQLiteParameter("@RefMarque", Article.Marque.RefMarque));
                Command.Parameters.Add(new SQLiteParameter("@PrixHT", Article.PrixHT));
                Command.Parameters.Add(new SQLiteParameter("@Quantite", Article.Quantite));
                int Status = Command.ExecuteNonQuery();

            }
        }

        public static List<Article> GetAllArticles() {

            var Articles = new List<Article>();
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT * FROM ArticleS";
                var Reader = Command.ExecuteReader();

                while (Reader.Read())
                {
                    var RefArticle = Reader.GetString(0);
                    var Description = Reader.GetString(1);
                    var SousFamille = Reader.GetInt32(2);
                    var Marque = Reader.GetInt32(3);
                    var PrixHt = Reader.GetFloat(4);
                    var Quantite = Reader.GetInt32(5);

                    SousFamille SousFam = SousFamilleDao.GetSousFamilleByReference(SousFamille);
                    Marque Mar = MarqueDao.GetMarqueByReference(Marque);
                  

                    Articles.Add(new Article(RefArticle, Description, Mar, SousFam, PrixHt, Quantite));
                }

                Reader.Close();

                SqliteConn.Close();

                return Articles;
            }
        }

        public static Article GetArticleByReference(String Reference)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT * FROM Articles WHERE RefArticle = @Reference";
                Command.Parameters.Add(new SQLiteParameter("@Reference", Reference));
                Command.ExecuteNonQuery();

                var Reader = Command.ExecuteReader();

                Article Article = new Article();

                while (Reader.Read())
                {
                    var ReferenceArticle = Reader.GetString(0);
                    var Description = Reader.GetString(1);
                    var SousFamille  = Reader.GetInt16(2);
                    var Marque = Reader.GetInt16(3);
                    var PrixHT = Reader.GetFloat(4);
                    var Quantite = Reader.GetInt16(5);

                    Article.RefArticle = ReferenceArticle;
                    Article.Description = Description;
                    Article.SousFamille = SousFamilleDao.GetSousFamilleByReference(SousFamille);
                    Article.Marque = MarqueDao.GetMarqueByReference(Marque);
                    Article.PrixHT = PrixHT;
                    Article.Quantite = Quantite;

                }

                SqliteConn.Close();

                return Article;
            }
        }

        public static List<Article> GetAllArticlesBySousFamille(string Reference)
        {

            var Articles = new List<Article>();
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT * FROM Articles WHERE RefSousFamille = @Ref";
                Command.Parameters.Add(new SQLiteParameter("@Ref", Reference));
                var Reader = Command.ExecuteReader();

                while (Reader.Read())
                {
                    var RefArticle = Reader.GetString(0);
                    var Description = Reader.GetString(1);
                    var SousFamille = Reader.GetInt32(2);
                    var Marque = Reader.GetInt32(3);
                    var PrixHt = Reader.GetFloat(4);
                    var Quantite = Reader.GetInt32(5);

                    SousFamille SousFam = SousFamilleDao.GetSousFamilleByReference(SousFamille);
                    Marque Mar = MarqueDao.GetMarqueByReference(Marque);


                    Articles.Add(new Article(RefArticle, Description, Mar, SousFam, PrixHt, Quantite));
                }

                Reader.Close();

                SqliteConn.Close();

                return Articles;
            }
        }


        public static List<Article> GetAllArticlesByMarque(long RefMarque)
        {
            var Articles = new List<Article>();
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "SELECT * FROM Articles WHERE RefMarque = @RefMarque";
                Command.Parameters.Add(new SQLiteParameter("@RefMarque", RefMarque));
                var Reader = Command.ExecuteReader();

                while (Reader.Read())
                {
                    var RefArticle = Reader.GetString(0);
                    var Description = Reader.GetString(1);
                    var SousFamille = Reader.GetInt32(2);
                    var Marque = Reader.GetInt32(3);
                    var PrixHt = Reader.GetFloat(4);
                    var Quantite = Reader.GetInt32(5);

                    SousFamille SousFam = SousFamilleDao.GetSousFamilleByReference(SousFamille);
                    Marque Mar = MarqueDao.GetMarqueByReference(Marque);


                    Articles.Add(new Article(RefArticle, Description, Mar, SousFam, PrixHt, Quantite));
                }

                Reader.Close();

                SqliteConn.Close();

                return Articles;
            }
        }

        public static void DeleteArticleByReference(String Reference)
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "DELETE FROM Articles WHERE RefArticle = @Reference ";
                Command.Parameters.Add(new SQLiteParameter("@Reference", Reference));
                Command.ExecuteNonQuery();

                SqliteConn.Close();
            }

        }

        public static void DeleteArticles()
        {
            using (SQLiteConnection SqliteConn = new SQLiteConnection(DbConnection))
            {
                SqliteConn.Open();
                SQLiteCommand Command = new SQLiteCommand(SqliteConn);
                Command.CommandText = "DELETE FROM Articles";
                Command.ExecuteNonQuery();

                SqliteConn.Close();
            }
        }
    }
}
