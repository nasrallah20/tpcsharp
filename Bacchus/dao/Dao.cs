﻿using System;
using System.Collections;
using System.Data;
using System.Data.SQLite;

namespace Bacchus.dao
{
    class Dao
    {
        protected static string DbConnection = "Data Source=Bacchus.SQLite;Version=3;";


        public static ArrayList GetTables()
        {
            ArrayList List = new ArrayList();

            // executes query that select names of all tables in master table of the database
            String Query = "SELECT name FROM sqlite_master " +
                    "WHERE type = 'table'" +
                    "ORDER BY 1";
            try
            {

                DataTable Table = GetDataTable(Query);

                // Return all table names in the ArrayList

                foreach (DataRow row in Table.Rows)
                {
                    List.Add(row.ItemArray[0].ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return List;
        }

        public static DataTable GetDataTable(string Sql)
        {
            try
            {
                DataTable Dt = new DataTable();
                using (var Connection = new SQLiteConnection(DbConnection))
                {
                    Connection.Open();
                    using (SQLiteCommand Cmd = new SQLiteCommand(Sql, Connection))
                    {
                        using (SQLiteDataReader rdr = Cmd.ExecuteReader())
                        {
                            Dt.Load(rdr);
                            return Dt;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public static DataSet GetDataSet()
        {
            DataSet Data = new DataSet();
            foreach (String TableName in GetTables())
            {
                Data.Tables.Add(GetDataTable("select * from " + TableName));
            }
            DataRelation relation1 =
               new System.Data.DataRelation("FamilleSousFamille",
               Data.Tables["Familles"].Columns["RefFamille"], Data.Tables["SousFamilles"].Columns["RefFamille"]);
            Data.Relations.Add(relation1);

            return Data;
        }

        
    }
}
