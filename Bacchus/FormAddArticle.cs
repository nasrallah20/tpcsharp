﻿using Bacchus.dao;
using System;
using System.Windows.Forms;

namespace Bacchus
{
    public partial class FormAddArticle : Form
    {
        public FormAddArticle()
        {
            InitializeComponent();

            FamilleCombobox.DataSource = FamilleDao.GetAllFamilles();
            FamilleCombobox.DisplayMember = "Nom";
            FamilleCombobox.ValueMember = "RefFamille";

            SousFamilleCombobox.DataSource = SousFamilleDao.GetAllSousFamilles();
            SousFamilleCombobox.DisplayMember = "Nom";
            SousFamilleCombobox.ValueMember = "Reference";

            MarqueCombobox.DataSource = MarqueDao.GetAllMarques();
            MarqueCombobox.DisplayMember = "Nom";
            MarqueCombobox.ValueMember = "RefMarque";
        }

        private void FamilleCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var nom = FamilleCombobox.GetItemText(FamilleCombobox.SelectedItem);
            Famille Famille = FamilleDao.GetFamilleByNom(nom);

            var SousFamilles = SousFamilleDao.GetSousFamilleByFamille(Famille);
            SousFamilleCombobox.DataSource = SousFamilles;
            SousFamilleCombobox.DisplayMember = "Nom";
            SousFamilleCombobox.ValueMember = "Reference";
        }

        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            if(ReferenceTextBox.Text != "" && FamilleCombobox.SelectedIndex > -1 && SousFamilleCombobox.SelectedIndex > -1 &&
                TextBoxQuantite.Text != "" && TextBoxPrix.Text != "" && RTextboxDescription.Text != "") {

                var Reference = ReferenceTextBox.Text;
                if(Reference.Length == 8)
                {
                    var Famille = FamilleCombobox.SelectedItem;

                    var SousFamille = (SousFamille)SousFamilleCombobox.SelectedItem;

                    var Marque = (Marque)MarqueCombobox.SelectedItem;

                    var Quantite = Convert.ToInt32(TextBoxQuantite.Text);

                    var PrixHT = Convert.ToInt32(TextBoxPrix.Text);
                    var Description = RTextboxDescription.Text;

                    Article Article = new Article(Reference, Description, Marque, SousFamille, PrixHT, Quantite);

                    ArticleDao.Persist(Article);
                    ((FormMain)Owner).RefreshWindow();
                    Close();

                }
                else
                {
                    MessageBox.Show("Le champ référence doit contenir 8 caractères", "Erreur", MessageBoxButtons.OK);

                }
            }
            else{
                MessageBox.Show("Vous devez remplir tous les champs", "Erreur", MessageBoxButtons.OK);
            }

        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
