﻿namespace Bacchus
{
    public class Marque
    {
        public string Nom { get; set; }
        public long RefMarque { get; set; }

        public Marque() { }

        public Marque(string Nom)
        {
            this.Nom = Nom;
        }

        public Marque(long Ref, string Nom){
            RefMarque = Ref;
            this.Nom = Nom;
        }
       
    }
}
