﻿namespace Bacchus
{
    public class SousFamille
    {
        public string Nom {get; set;}
        public Famille Famille { get; set; }
        public long Reference { get; set; }

        public SousFamille()
        {

        }

        public SousFamille(Famille famille, string Nom)
        {
            this.Nom = Nom;
            this.Famille = famille;
        }

        public SousFamille(long Reference, Famille famille,string Nom)
        {
            this.Reference = Reference;
            this.Nom = Nom;
            this.Famille = famille;
        }

    }
}
