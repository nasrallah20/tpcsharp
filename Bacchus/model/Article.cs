﻿namespace Bacchus
{
    public class Article
    {
        public string RefArticle {get; set;}
        public SousFamille SousFamille {get; set;}
        public string Description {get; set;}
        public Marque Marque {get; set;}
        public long Quantite {get; set;}
        public double PrixHT {get; set;}

        public Article()
        {

        }

        public Article(string Ref, string Description, Marque Marque, SousFamille SousFamille, double PrixHT, int Quantite)
        {
            this.RefArticle = Ref;
            this.SousFamille = SousFamille;
            this.Description = Description;
            this.PrixHT = PrixHT;
            this.Marque = Marque;
            this.Quantite = Quantite;
        }
    }
}
