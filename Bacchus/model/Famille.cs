﻿namespace Bacchus
{
    public class Famille
    {
        public long RefFamille { get; set; }
        public string Nom { get; set; }

        public Famille() { }

        public Famille(string Nom)
        {
            this.Nom = Nom;
        }

        public Famille(int Ref,string Nom)
        {
            RefFamille = Ref;
            this.Nom = Nom;
        }
    }
}
