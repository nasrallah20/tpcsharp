﻿using Bacchus.dao;
using System;
using System.Windows.Forms;

namespace Bacchus
{
    public partial class FormAddSousFamille : Form
    {
        public FormAddSousFamille()
        {
            InitializeComponent();

            FamilleCombobox.DataSource = FamilleDao.GetAllFamilles();
            FamilleCombobox.DisplayMember = "Nom";
            FamilleCombobox.ValueMember = "RefFamille";
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Confirmer_Click(object sender, EventArgs e)
        {
            if (NomTextBox.Text != "" && FamilleCombobox.SelectedIndex > -1)
            {
                var Nom = NomTextBox.Text;
                long FamilleRef = long.Parse(FamilleCombobox.SelectedValue.ToString());

                Famille Famille = FamilleDao.GetFamilleByReference(FamilleRef);

                SousFamille SousFamille = new SousFamille(-1, Famille, Nom);

                SousFamilleDao.Persist(SousFamille);

                ((FormMain)Owner).RefreshWindow();
                Close();

            }
            else
            {
                MessageBox.Show("Vous devez remplir tous les champs", "Erreur", MessageBoxButtons.OK);
            }
        }
    }
}
