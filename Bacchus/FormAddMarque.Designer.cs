﻿namespace Bacchus
{
    partial class FormAddMarque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Confirmer = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.NomTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Confirmer
            // 
            this.Confirmer.Location = new System.Drawing.Point(335, 142);
            this.Confirmer.Name = "Confirmer";
            this.Confirmer.Size = new System.Drawing.Size(104, 40);
            this.Confirmer.TabIndex = 11;
            this.Confirmer.Text = "Confirmer";
            this.Confirmer.UseVisualStyleBackColor = true;
            this.Confirmer.Click += new System.EventHandler(this.Confirmer_Click);
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(210, 142);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(104, 40);
            this.Cancel.TabIndex = 10;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // NomTextBox
            // 
            this.NomTextBox.Location = new System.Drawing.Point(149, 66);
            this.NomTextBox.Name = "NomTextBox";
            this.NomTextBox.Size = new System.Drawing.Size(290, 31);
            this.NomTextBox.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 25);
            this.label2.TabIndex = 7;
            this.label2.Text = "Nom";
            // 
            // FormAddMarque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 208);
            this.Controls.Add(this.Confirmer);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.NomTextBox);
            this.Controls.Add(this.label2);
            this.Name = "FormAddMarque";
            this.Text = "Ajouter une marque";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Confirmer;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.TextBox NomTextBox;
        private System.Windows.Forms.Label label2;
    }
}