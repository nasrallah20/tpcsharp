﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bacchus
{
    public partial class FormExport : Form
    {
        public FormExport()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = "Document"; // Default file name
            sfd.DefaultExt = ".csv"; // Default file extension
            sfd.Filter = "CSV Files (*.csv)|*.csv"; // Filter files by extension
            sfd.Title = "Exporter dans un fichier csv";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                Stream stream = sfd.OpenFile();
                string path = sfd.FileName.ToString();
                textBox1.Text = path;
                stream.Close();
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(textBox1.Text != "")
            {
                gestionFichier.Export.ExportBddEnCSV(textBox1.Text, progressBar1);
                MessageBox.Show("Exportation réussie");
            }
            else
            {
                MessageBox.Show("Il faut choisir un fichier avant d'exporter");
            }
            
        }
    }
}
