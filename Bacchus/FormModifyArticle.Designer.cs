﻿namespace Bacchus
{
    partial class FormModifyArticle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MarqueCombobox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.CancelButton = new System.Windows.Forms.Button();
            this.ConfirmButton = new System.Windows.Forms.Button();
            this.TextBoxPrix = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TextBoxQuantite = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.RTextboxDescription = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SousFamilleCombobox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.FamilleCombobox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ReferenceTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // MarqueCombobox
            // 
            this.MarqueCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MarqueCombobox.FormattingEnabled = true;
            this.MarqueCombobox.Location = new System.Drawing.Point(238, 322);
            this.MarqueCombobox.Name = "MarqueCombobox";
            this.MarqueCombobox.Size = new System.Drawing.Size(266, 33);
            this.MarqueCombobox.TabIndex = 33;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(80, 325);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 25);
            this.label7.TabIndex = 32;
            this.label7.Text = "Marque :";
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(304, 761);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(109, 38);
            this.CancelButton.TabIndex = 31;
            this.CancelButton.Text = "Annuler";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ConfirmButton
            // 
            this.ConfirmButton.Location = new System.Drawing.Point(175, 761);
            this.ConfirmButton.Name = "ConfirmButton";
            this.ConfirmButton.Size = new System.Drawing.Size(123, 39);
            this.ConfirmButton.TabIndex = 30;
            this.ConfirmButton.Text = "Confirmer";
            this.ConfirmButton.UseVisualStyleBackColor = true;
            this.ConfirmButton.Click += new System.EventHandler(this.Confirmer_Click);
            // 
            // TextBoxPrix
            // 
            this.TextBoxPrix.Location = new System.Drawing.Point(238, 668);
            this.TextBoxPrix.Name = "TextBoxPrix";
            this.TextBoxPrix.Size = new System.Drawing.Size(266, 31);
            this.TextBoxPrix.TabIndex = 29;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(80, 671);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 25);
            this.label6.TabIndex = 28;
            this.label6.Text = "Prix HT :";
            // 
            // TextBoxQuantite
            // 
            this.TextBoxQuantite.Location = new System.Drawing.Point(238, 586);
            this.TextBoxQuantite.Name = "TextBoxQuantite";
            this.TextBoxQuantite.Size = new System.Drawing.Size(266, 31);
            this.TextBoxQuantite.TabIndex = 27;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(80, 589);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 25);
            this.label5.TabIndex = 26;
            this.label5.Text = "Quantité :";
            // 
            // RTextboxDescription
            // 
            this.RTextboxDescription.Location = new System.Drawing.Point(238, 409);
            this.RTextboxDescription.Name = "RTextboxDescription";
            this.RTextboxDescription.Size = new System.Drawing.Size(266, 138);
            this.RTextboxDescription.TabIndex = 25;
            this.RTextboxDescription.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(80, 451);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 25);
            this.label2.TabIndex = 24;
            this.label2.Text = "Description :";
            // 
            // SousFamilleCombobox
            // 
            this.SousFamilleCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SousFamilleCombobox.FormattingEnabled = true;
            this.SousFamilleCombobox.Location = new System.Drawing.Point(238, 228);
            this.SousFamilleCombobox.Name = "SousFamilleCombobox";
            this.SousFamilleCombobox.Size = new System.Drawing.Size(266, 33);
            this.SousFamilleCombobox.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(80, 231);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 25);
            this.label4.TabIndex = 22;
            this.label4.Text = "Sous-famille :";
            // 
            // FamilleCombobox
            // 
            this.FamilleCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FamilleCombobox.FormattingEnabled = true;
            this.FamilleCombobox.Location = new System.Drawing.Point(238, 136);
            this.FamilleCombobox.Name = "FamilleCombobox";
            this.FamilleCombobox.Size = new System.Drawing.Size(266, 33);
            this.FamilleCombobox.TabIndex = 21;
            this.FamilleCombobox.SelectedIndexChanged += new System.EventHandler(this.FamilleCombobox_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(80, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 25);
            this.label3.TabIndex = 20;
            this.label3.Text = "Famille :";
            // 
            // ReferenceTextBox
            // 
            this.ReferenceTextBox.Location = new System.Drawing.Point(238, 51);
            this.ReferenceTextBox.Name = "ReferenceTextBox";
            this.ReferenceTextBox.ReadOnly = true;
            this.ReferenceTextBox.Size = new System.Drawing.Size(266, 31);
            this.ReferenceTextBox.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(80, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 25);
            this.label1.TabIndex = 18;
            this.label1.Text = "Reference :";
            // 
            // FormModifyArticle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 822);
            this.Controls.Add(this.MarqueCombobox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.ConfirmButton);
            this.Controls.Add(this.TextBoxPrix);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TextBoxQuantite);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.RTextboxDescription);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.SousFamilleCombobox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.FamilleCombobox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ReferenceTextBox);
            this.Controls.Add(this.label1);
            this.Name = "FormModifyArticle";
            this.Text = "Modifier un article";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox MarqueCombobox;
        private System.Windows.Forms.Label label7;
        private new System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button ConfirmButton;
        private System.Windows.Forms.TextBox TextBoxPrix;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TextBoxQuantite;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox RTextboxDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox SousFamilleCombobox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox FamilleCombobox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ReferenceTextBox;
        private System.Windows.Forms.Label label1;
    }
}